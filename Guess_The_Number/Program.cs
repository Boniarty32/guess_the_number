﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

//1)комп рандомирует число (1-100) +
//2)комп предлагает пользователю отгадать это число +
//3)пользователь вводи своё число +
//4)комп сравнивает число своё и пользователя +
//4.1)если число пользователя больше числа компа, то комп преллагает ввести поменьше +
//4.2)если число пользователя меньше числа компа, то комп преллагает ввести побольше + 
//Повторяем шаги 2,3,4 пока пользователь не введет число равное числу компа+ 

//ДОПОЛНЕНИЯ 
//1)сделать уровень слодности, в зависимости отнего будет изменять отрезорк загадывания числа компом +
//2)сделать подсчёт за сколько попыто пользователь отгадал число +
//3)сделать обратный отчсчёт если пользователь не успел отгадать за Х попыток он проиграл +
//4)сделать оценку игрока в зависимости от того за сколько пыток он угадал число +
//5)1..100, 1..100 плавающие границы + 
//56 1..100 
//40 40..100 
//80 40..80 
//5.1)провека на ввод чисел за границе отрезка +
//6)сделать красивую консоль - поменять цвета текста  +
//7)защита от тупого ввода(если вводят буквы не вылетает) +


namespace Guess_The_Number
{
    class Program
    {
        static void Main(string[] args)
        {
            Random R_Num = new Random();

            bool continueGame;

            do
            {
                    int random, Player_Num = 0;
                    string winMassage = "0";

                    int false_Counter = 10;
                    int level = 1;

                    
                    int counter = 0;

                // ПРОМЕЖУТОК РАНДОМАТОРА МЕНЯТЬ ЗДЕСЬ!
                    int Num1 = 0;
                    int Num2 = 100;
                ////
                    Console.ForegroundColor = ConsoleColor.Blue;
                    Console.WriteLine("Нажмите Enter что бы начать игру");
                    Console.ReadKey();
                
                

                    random = R_Num.Next(Num1, Num2);
                    Console.WriteLine("Ваша задача отгадать загаданное компьютером число");

                    Console.ForegroundColor = ConsoleColor.Green;
                    Console.WriteLine($"От {Num1} до {Num2}");

               
                // уровень сложности
                try
                {
                    do
                    {
                        Console.ForegroundColor = ConsoleColor.Yellow;
                        Console.WriteLine("Выберите уровень сложности 1 -- 2 -- 3");
                        level = int.Parse(Console.ReadLine());

                        Console.ForegroundColor = ConsoleColor.Red;
                        if (level == 1)
                        {
                            false_Counter = 10;
                            Console.WriteLine("У вас 10 попыток");
                        }
                        else if (level == 2)
                        {
                            false_Counter = 5;
                            Console.WriteLine("У вас 5 попыток");
                        }
                        else if (level == 3)
                        {
                            false_Counter = 3;
                            Console.WriteLine("У вас 3 попытки");
                        }
                        else if ((level > 3) || (level < 1))
                        {
                            Console.WriteLine("Такого уровня не существует");
                        }
                        else
                        {
                            Console.WriteLine("Вводите только числа");
                        }

                    } while ((level >= 4 ) || (level <= 0));

                } catch
                {
                    Console.WriteLine("Вводите только числа");
                    
                }
                ////


                // для теста // Console.WriteLine($"{random}");


                // цикл игры
                while (Player_Num != random)
                {
                    try
                    {
                        Console.ForegroundColor = ConsoleColor.Green;
                        Console.Write("Ваше число: ");
                        Player_Num = int.Parse(Console.ReadLine());

                        if (Player_Num < Num1 || Player_Num > Num2)
                        {
                            Console.ForegroundColor = ConsoleColor.DarkRed;
                            Console.WriteLine("Вводите числа указанные выше");

                        }

                        if (Player_Num > random && Player_Num <= Num2)
                        {
                            counter++;
                            false_Counter--;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($" Осталось {false_Counter} попыток");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("Введите число поменьше");
                            Num2 = Player_Num;
                            Console.WriteLine($" В промежутке c {Num1} по {Num2}");

                        }
                        else if (Player_Num < random && Player_Num >= Num1)
                        {
                            counter++;
                            false_Counter--;
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($" Осталось {false_Counter} попыток");
                            Console.ForegroundColor = ConsoleColor.Yellow;
                            Console.WriteLine("Введите число побольше");
                            Num1 = Player_Num;
                            Console.WriteLine($" В промежутке c {Num1} по {Num2}");

                        }
                       
                        else if (Player_Num == random)
                        {
                            counter++;
                            Console.ForegroundColor = ConsoleColor.Magenta;
                            Console.WriteLine($"Вы выиграли!!!!!!!! c {counter} попытки");
                            Console.ForegroundColor = ConsoleColor.Cyan;

                            // Победные звания 

                            if (counter <= 2 && counter > 0)
                            { Console.WriteLine("ГЕНИЙ ");}
                            else if (counter >= 3 && false_Counter != 0 && counter <= 5)
                            { Console.WriteLine("СПОСОБНЫЙ"); }
                            else if (counter >= 6 && false_Counter != 0 && counter <= 8)
                            { Console.WriteLine("ТАК СЕБЕ РЕЗУЛЬТАТ"); }
                            else if (counter >= 9 && false_Counter != 0 && counter <= 10)
                            { Console.WriteLine("ИГРЫ ЭТО НЕ ТВОЕ"); }
                            ////
                            
                        }

                        
                        if (false_Counter == 0)
                        {
                            Console.ForegroundColor = ConsoleColor.Red;
                            Console.WriteLine($"У вас не осталось попыток!!!!! Вы проиграли!!!!!");
                            Console.ForegroundColor = ConsoleColor.Cyan;
                            Console.WriteLine("ЛУЗЕР");
                            Player_Num = random;
                        }
                        
                    }
                    catch
                    {
                        Console.WriteLine("Вводите только числа");
                        
                    }
                }
                

                Console.ForegroundColor = ConsoleColor.Blue;
                Console.WriteLine(" Хотите сыграть еше ? (y/n): ");
                char choice = char.Parse(Console.ReadLine());
                if (choice == 'y')
                {
                    continueGame = true;
                }
                else
                {
                    continueGame = false;
                }


            } while (continueGame == true);

            ////

            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("Очень жаль, до скорой встречи!!!");
            Console.ReadKey();

            

        }
    }
}
